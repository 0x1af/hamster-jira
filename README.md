# hamster-jira

A little python script to manually export logged work from hamster to jira

Note: There also exist superior projects:
* https://github.com/kraiz/hamster-bridge 
* https://github.com/mgaitan/hamster2jira

However, if you prefer to export logged work manually just once a day (or less :-) ),
you might have come to the right spot.


So far this script has been only tested with:
* python 2.7
* hamster-time-tracker 1.04
* jira 1.0.8 (jira for python, installed via pip)

```
usage: hamster_jira.py [-h] [-d] [-c CONFIG_PATH] [-t] [-y] [-s START_DATE]
                       [-e END_DATE]

optional arguments:
  -h, --help            show this help message and exit
  -d, --dry-run
  -c CONFIG_PATH, --config CONFIG_PATH
  -t, --today
  -y, --yesterday
  -s START_DATE, --start START_DATE
                        format '%Y-%m-%d'
  -e END_DATE, --end END_DATE
                        format '%Y-%m-%d'
```



The corresponding jira issue ID is taken from the activity name, or any tags which have been set for the activity.
If no jira issue ID could be found for an activity, the script will ask for one (press ENTER to skip or 'q' to quit).


Unwanted issue IDs can be added to a blacklist in the config file.
In that case when encountered in an activity name or tag, they are ignored.


A config file is required (default path '~/.hamster_jira.cfg'). It may look like:
 
```
[login]
user=me
pwd=mine

[jira]
url=http://my.jira

[issue]
blacklist=SET-01,SOL-02

[proxy]
http=
https=
```



