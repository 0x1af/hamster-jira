#!/usr/bin/env python2.7

from jira import JIRA
import calendar
from datetime import date, datetime, timedelta
from dateutil.parser import parse as dateutil_parse
import argparse
import sys
import inspect
from hamster.configuration import runtime
import re
from tzlocal import get_localzone # see issue: https://community.atlassian.com/t5/Jira-questions/python-jira-add-worklog-giving-internal-error/qaq-p/25548
import ConfigParser
import os

def date_format():
	return "%Y-%m-%d"

def time_format():
	return "%H:%M"

def date2str(datetime_val):
	return datetime_val.strftime(date_format())

def time2str(datetime_val):
	return datetime_val.strftime(time_format())

def help_sample_config():
	sample_config_file='''
[login]
user=me
pwd=mine

[jira]
url=http://my.jira

[issue]
blacklist=SET-01,SOL-02

[proxy]
http=
https='''
	return sample_config_file

def help_epilog(config_default_path, help_sample_config):
	epilog='''
Press ENTER to skip an issue, or 'q' + ENTER to quit.

A config file is required (default path '%s').
It may look like:
%s
'''%(config_default_path, help_sample_config)
	return epilog

def load_config(config_path):
	config = ConfigParser.SafeConfigParser()
	realpath = os.path.realpath(os.path.expanduser(config_path))
	nread = config.read(realpath)
	if len(nread) < 1:
		raise Exception("Config file not readable '%s'"%(realpath))
	cfg_dict = {}
	for section in config.sections():
		cfg_dict[section] = dict(config.items(section))
	cfg_dict["issue"]["blacklist"] = cfg_dict["issue"]["blacklist"].split()
	return cfg_dict

def user_input(desc, default="y"):
	answer = raw_input(desc).strip()
	if answer == 'q':
		exit(0)
	elif answer == 'n':
		answer = "" # skip to next
	elif answer == "":
		answer = default
	return answer

class TodayAction(argparse.Action):
	def __call__(self, parser, namespace, values, option_string=None):
		setattr(namespace, 'start_date',  date.today())
		setattr(namespace, 'end_date',  date.today())

class YesterdayAction(argparse.Action):
	def __call__(self, parser, namespace, values, option_string=None):
		yesterday = date.today() - timedelta(days = 1)
		setattr(namespace, 'start_date',  yesterday)
		setattr(namespace, 'end_date',  yesterday)

def valid_date_type(arg_date_str):
	# taken from: monkut/argparse_date_datetime_custom_types.py (https://gist.github.com/monkut/e60eea811ef085a6540f)
	"""custom argparse *date* type for user dates values given from the command line"""
	try:
		return datetime.strptime(arg_date_str, date_format())
	except ValueError:
		raise argparse.ArgumentTypeError("Given Date ({0}) not valid! Expected format, YYYY-MM-DD!".format(arg_date_str))

def parse_args():
	config_default_path = '~/.hamster_jira.cfg'
	

	parser = argparse.ArgumentParser(
		epilog=help_epilog(config_default_path, help_sample_config()),
		formatter_class=argparse.RawTextHelpFormatter
	)
	parser.add_argument('-d', '--dry-run', dest='dry_run', action='store_true')
	parser.add_argument('-c', '--config', dest='config_path', default=config_default_path)
	
	date_format_help = "format '%s'"%(date_format().replace('%', '%%'))

	parser.add_argument('-t', '--today', dest='today', action=TodayAction, nargs=0)
	parser.add_argument('-y', '--yesterday', dest='yesterday', action=YesterdayAction, nargs=0)
	parser.add_argument('-s', '--start', dest='start_date', type=valid_date_type, action='store', help=date_format_help)
	parser.add_argument('-e', '--end', dest='end_date', type=valid_date_type, action='store', help=date_format_help)
	args = parser.parse_args()

	if args.start_date is None:
		setattr(args, 'start_date',  date.today())
	if args.end_date is None:
		setattr(args, 'end_date',  date.today())
	return args

def extract_jira_issue_key(activity_name, tags, issue_key_blacklist):
	issue_key_rgx = re.compile("[A-Z]{1,3}-[0-9]{1,}")

	issue_key = ""
	# check activity name for issue key
	match = re.match(issue_key_rgx, activity_name)
	if match and match.group(0) not in issue_key_blacklist:
		issue_key = match.group(0)
	# if none was found, check the associated tags
	if not issue_key:
		issue_key_tags = []
		for tag in tags:
			match = issue_key_rgx.match(tag)
			if match and match.group(0) not in issue_key_blacklist:
				issue_key_tags.append(match.group(0))

		if len(issue_key_tags) > 0:
			issue_key = issue_key_tags[0].group(0)
		if len(issue_key_tags) > 1:
			print("Warning: multiple key tags found:")
			print(issue_key_tags)
	return issue_key

def has_worklog_entry_with_starttime(issue, fact_started):
	for entry in issue.fields.worklog.worklogs:
		entry_started  = dateutil_parse(entry.started)

		if fact_started == entry_started:
			return True
	return False

def export_facts_to_jira(jira, facts, config, args):
	fact_issue_matching = {}
	tz = get_localzone()

	for fact in facts:
		print("----------------------")
		#print(inspect.getmembers(fact))

		if not fact.end_time:
			print("Fact without end_time: %s, %s -  %s "%(date2str(fact.start_time), time2str(fact.start_time), fact.activity))
			continue

		total_seconds = (fact.end_time-fact.start_time).total_seconds()
			
		print("%s, %s - %s (%d min): %s "%(date2str(fact.start_time), time2str(fact.start_time), time2str(fact.end_time), total_seconds/60,  fact.activity))
		issue_key = extract_jira_issue_key(fact.activity, fact.tags, config["issue"]["blacklist"])

		if not issue_key and fact.activity in fact_issue_matching:
			answer = user_input("Re-use issue key '%s' (y)?"%(fact_issue_matching[fact.activity]))
			if str.lower(answer) == 'y':
				issue_key = fact_issue_matching[fact.activity]
		if not issue_key:
			issue_key = user_input("Enter issue key: ", "").strip()
			if not issue_key:
				print("Skipping issue ...")
				continue
		fact_issue_matching[fact.activity] = issue_key
		
		
		issue = jira.issue(issue_key)
		#print(issue.fields.summary)
		#print(inspect.getmembers(issue))
		fact_started = tz.localize(fact.start_time)

		if has_worklog_entry_with_starttime(issue, fact_started):
			print("Found work log entry with same start time %s. Skipping ..."%(str(fact.start_time)))
			continue
	
		#print(inspect.getmembers(entry))
		
		#, timeSpent=None, timeSpentSeconds=None, adjustEstimate=None, newEstimate=None, reduceBy=None, comment=None, started=None, user=None)
		
		comment = fact.activity
		if fact.description:
			comment += ", "+str(fact.description)

		print("'%s' - '%s' - Adding %d sec, start-time: %s"%(issue_key, issue.fields.summary, total_seconds, str(fact.start_time)))
		
		if not args.dry_run:
			jira.add_worklog(issue_key, timeSpentSeconds=total_seconds, comment=comment, started=fact_started)
		else:
			print("* dry run *")

if __name__ == '__main__':
	args = parse_args()
	config = load_config(args.config_path)

	jira = JIRA(config["jira"]["url"], proxies=config["proxy"], basic_auth=(config["login"]["user"], config["login"]["pwd"]) )
	facts = runtime.storage.get_facts(args.start_date, args.end_date)

	if not facts:
		print("No hamster facts found for the given time range (%s - %s)."%(date2str(args.start_date), date2str(args.end_date)))
	else:
		export_facts_to_jira(jira, facts, config, args)
